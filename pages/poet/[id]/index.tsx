import { NextPage } from 'next';

import AvatarCover from '@Components/common/cover/cover-avatar';
import Feature from '@Components/common/feature/feature-small-4';
import Section from '@Components/common/section';
import Layout from '@Components/layout';
import { instance } from '@Utils/axios';
import { Book, PoetPage } from '@Utils/types';

const PagePoet: NextPage<{ data: PoetPage }> = ({ data }) => {
  return (
    <Layout title={`غزل گو | ${data.name}`}>
      <AvatarCover
        avatar={data.profile?.location}
        name={data.name}
        desc={data.biography}
      />
      <Section className="border--bottom">
        <div className="container">
          <h2>کتاب ها</h2>
          <div className="row">
            {data.books.map((val: Book) => (
              <Feature
                title={val.name}
                desc={val.description}
                button={{
                  link: "/book/[id]",
                  title: "نمایش کتاب",
                  as: `/book/${val.slogan}`
                }}
              />
            ))}
          </div>
        </div>
      </Section>
    </Layout>
  );
};

PagePoet.getInitialProps = async ({ query }) => {
  const { data } = await instance.get<PoetPage>(`poet/${query.id}`);

  return { data };
};

export default PagePoet;
