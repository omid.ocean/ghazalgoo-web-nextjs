import { NextPage } from 'next';

import ImageCover from '@Components/common/cover/cover-image-1';
import Feature from '@Components/common/feature/feature-small-2';
import Tab from '@Components/common/tabs/tab';
import Tabs from '@Components/common/tabs/tabs';
import Layout from '@Components/layout';
import { instance } from '@Utils/axios';
import { BookPage, SubChapter, SubPoem } from '@Utils/types';

const PageBook: NextPage<{ data: BookPage }> = ({ data }) => {
  return (
    <Layout title={`غزل گو | ${data.name}`}>
      <ImageCover
        title={data.name}
        lead={data.description}
        backGround={data.cover?.location}
        short={`نویسنده : ${data.poet.name}`}
      />
      <Tabs type="vertical">
        {data.chapters.map((val: SubChapter) => (
          <Tab title={val.name} key={val.slogan}>
            <div className="row">
              {val.poems.map((poem: SubPoem, index: number) => (
                <Feature
                  linked={{ as: `/poem/${poem.slogan}`, link: "/poem/[id]" }}
                  title={poem.name}
                  desc={poem.description}
                  number={index + 1}
                />
              ))}
            </div>
          </Tab>
        ))}
      </Tabs>
    </Layout>
  );
};

PageBook.getInitialProps = async ({ query }) => {
  const { data } = await instance.get<BookPage>(`book/${query.id}`);

  return { data };
};

export default PageBook;
