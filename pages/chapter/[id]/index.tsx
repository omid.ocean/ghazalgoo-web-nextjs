import { NextPage } from 'next';

import ImageCover from '@Components/common/cover/cover-image-1';
import Feature from '@Components/common/feature/feature-small-2';
import Section from '@Components/common/section';
import Layout from '@Components/layout';

const PageChapter: NextPage<{ userAgent: string }> = () => {
  return (
    <Layout title="غزل گو">
      <ImageCover
        title="سلام!"
        lead="سلام سلام"
        backGround="gg"
        short="نویسنده : نام شاعر"
      />
      <Section>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <Feature
                linked={{ as: "/", link: "/" }}
                title="تست "
                desc="سسنیتبلیسشتا بتاشیلبتشیسلبتشسلی بنتشسلیب انتشسلین بتشس ب"
                number={6}
              />
              <Feature
                linked={{ as: "/", link: "/" }}
                title="تست "
                desc="سسنیتبلیسشتا بتاشیلبتشیسلبتشسلی بنتشسلیب انتشسلین بتشس ب"
                number={6}
              />
              <Feature
                linked={{ as: "/", link: "/" }}
                title="تست "
                desc="سسنیتبلیسشتا بتاشیلبتشیسلبتشسلی بنتشسلیب انتشسلین بتشس ب"
                number={6}
              />
            </div>
          </div>
        </div>
      </Section>
    </Layout>
  );
};

PageChapter.getInitialProps = async ({ req }) => {
  const userAgent = req ? req.headers["user-agent"] || "" : navigator.userAgent;
  return { userAgent };
};

export default PageChapter;
