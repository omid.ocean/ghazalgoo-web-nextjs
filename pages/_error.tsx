import { NextPage } from 'next';
import Link from 'next/link';
import React from 'react';

import Layout from '@Components/layout';

interface IError {
  statusCode: number | undefined;
}

const Error: NextPage<IError> = ({ statusCode }) => {
  return (
    <Layout title="error">
      <section className="height-100 bg--dark text-center">
        <div className="container pos-vertical-center">
          <div className="row">
            <div className="col-md-12">
              <h1 className="h1--large">{statusCode}</h1>
              <p className="lead">
                The page you were looking for was not found.
              </p>
              <Link href="/">
                <a>Go back to home pages</a>
              </Link>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};
Error.getInitialProps = async ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

export default Error;
