import { NextPage } from 'next';

import Layout from '@Components/layout';
import HomeHeader from '@Components/searchHeader';

const PageDonate: NextPage<{ userAgent: string }> = () => {
  return (
    <Layout title="غزل گو">
      <HomeHeader
        title="آثار سخنسرایان پارسی‌گو"
        subTitle="در گنجینه آثار جست و جو نمایید"
        onSubmit={s => console.log(s)}
      />
    </Layout>
  );
};

PageDonate.getInitialProps = async ({ req }) => {
  const userAgent = req ? req.headers["user-agent"] || "" : navigator.userAgent;
  return { userAgent };
};

export default PageDonate;
