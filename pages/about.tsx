import { NextPage } from 'next';

import Cover from '@Components/common/cover/cover-text-4';
import Layout from '@Components/layout';

const PageAbout: NextPage<{ userAgent: string }> = () => {
  return (
    <Layout title="غزل گو">
      <Cover
        bg="white"
        title="درباره ما"
        content="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک  حال و آینده شناخت مورد استفاده قرار گیرد."
        coverImage="https://via.placeholder.com/900"
      />
    </Layout>
  );
};

PageAbout.getInitialProps = async ({ req }) => {
  const userAgent = req ? req.headers["user-agent"] || "" : navigator.userAgent;
  return { userAgent };
};

export default PageAbout;
