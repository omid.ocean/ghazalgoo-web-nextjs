import { NextPage } from 'next';

import Layout from '@Components/layout';
import HomeHeader from '@Components/searchHeader';

import ArtistsList from '../components/HomeArtistsList';

const PageHome: NextPage<{}> = () => {
  return (
    <Layout title="غزل گو">
      <HomeHeader
        title="آثار سخنسرایان پارسی‌گو"
        subTitle="در گنجینه آثار جست و جو نمایید"
        onSubmit={s => console.log(s)}
      />
      <ArtistsList />
    </Layout>
  );
};

export default PageHome;
