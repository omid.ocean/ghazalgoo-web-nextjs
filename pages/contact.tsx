import { NextPage } from 'next';

import Contact from '@Components/common/contact/form-with-text';
import PageTitle from '@Components/common/titles/page-title-1';
import Layout from '@Components/layout';

const PageContact: NextPage<{ userAgent: string }> = () => {
  return (
    <Layout title="غزل گو">
      <PageTitle title="تماس با ما" lead="با ما در تماس باشید!" bg="primary" />
      <Contact bg="white" desc="تست" phone="sss" email="ssss" time="sss" />
    </Layout>
  );
};

export default PageContact;
