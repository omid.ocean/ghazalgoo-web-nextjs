import { ModalProvider } from 'hooks/modal';
import { SettingsProvider } from 'hooks/settings';
import localforage from 'localforage';
import { PageTransition } from 'next-page-transitions';
import App from 'next/app';
import Head from 'next/head';
import * as React from 'react';
import { ThemeProvider } from 'styled-components';
import uuidv4 from 'uuid/v4';

import Loading from '@Components/common/loading';
import ModalContainer from '@Components/common/modal';
import Footer from '@Components/footer';
import NavBar from '@Components/navbar';
import Theme from '@Components/theme';

const theme = {
  colors: {
    primary: "#0070f3"
  }
};
class MyApp extends App {
  state = {
    isRouteChanging: false,
    loadingKey: undefined
  };

  componentDidMount() {
    localforage.config({
      name: "Ghazalgoo web app",
      storeName: "savedData",
      version: 1.1,
      description: "main storage used for saving poems"
    });

    const { router } = this.props;

    const routeChangeStartHandler = () => {
      this.setState(() => ({
        isRouteChanging: true,
        loadingKey: uuidv4().substr(0, 8)
      }));
    };

    const routeChangeEndHandler = () => {
      this.setState(() => ({
        isRouteChanging: false
      }));
    };

    router.events.on("routeChangeStart", routeChangeStartHandler);
    router.events.on("routeChangeComplete", routeChangeEndHandler);
    router.events.on("routeChangeError", routeChangeEndHandler);
  }

  public render() {
    const { Component, pageProps, router } = this.props;

    return (
      <div>
        <Head>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />
          <title>غزل گو</title>
        </Head>
        <SettingsProvider>
          <ModalProvider>
            <ThemeProvider theme={theme}>
              <Loading {...this.state} />
              <Theme />
              <NavBar />

              <PageTransition
                skipInitialTransition
                timeout={300}
                classNames="page-transition"
              >
                <Component {...pageProps} key={router.route} />
              </PageTransition>
              <Footer />
              <ModalContainer />
            </ThemeProvider>
          </ModalProvider>
        </SettingsProvider>
      </div>
    );
  }
}

export default MyApp;
