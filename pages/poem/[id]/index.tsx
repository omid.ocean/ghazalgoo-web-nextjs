import localforage from 'localforage';
import { NextPage } from 'next';
import Link from 'next/link';
import { useEffect, useState } from 'react';

import Comments from '@Components/common/comments/commetns';
import IconButton from '@Components/common/icon-button';
import Section from '@Components/common/section';
import Space from '@Components/common/space';
import Layout from '@Components/layout';
import Verses from '@Components/verse';
import { instance } from '@Utils/axios';
import { PoemPage, SubTag } from '@Utils/types';

const PagePoem: NextPage<{
  data: PoemPage;
  pageID: string;
  availableOffline: boolean;
}> = ({ data, pageID, availableOffline }) => {
  const [offline, setOffline] = useState(availableOffline);

  useEffect(() => {
    localforage.getItem(`poem-${pageID}`).then(val => {
      if (val) {
        setOffline(true);
      }
    });
  }, []);
  const addOffline = () => {
    localforage.setItem(`poem-${pageID}`, data);
    setOffline(true);
  };

  const removeOffline = () => {
    localforage.removeItem(`poem-${pageID}`);
    setOffline(false);
  };

  return (
    <Layout title={`غزل گو | ${data.name}`}>
      <Section bg="white">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-10 col-lg-8">
              <article>
                <div className="article__title text-center">
                  <h1 className="h2">{data.name}</h1>
                  <span>
                    نویسنده :
                    <Link
                      href="/poet/[id]"
                      as={`/poet/${data.chapter.poet.slogan}`}
                    >
                      <a style={{ textDecoration: "none" }}>
                        {data.chapter.poet.name}
                      </a>
                    </Link>
                  </span>
                  {data.tags.length > 0 && (
                    <span>
                      /
                      {data.tags.map((val: SubTag) => (
                        <a href="#"> {val.content} </a>
                      ))}
                    </span>
                  )}
                </div>
                <div className="article__body">
                  <img
                    alt="تصویر "
                    src={
                      data.cover
                        ? data.cover.location
                        : "/static/images/poemBG.jpg"
                    }
                  />
                  <Verses verses={data.verses} />
                </div>
                <div className="article__share text-center">
                  <IconButton
                    title={offline ? "حذف نسخه افلاین" : "ذخیره افلاین"}
                    iconClass="icon-Save"
                    onClick={offline ? removeOffline : addOffline}
                  />
                  <IconButton
                    title="اضافه کردن به علاقه مندی ها"
                    link="#"
                    iconClass="icon-Heart-2"
                  />
                  <IconButton
                    title="ارسال به دوست"
                    link="#"
                    iconClass="icon-Sharethis"
                  />
                </div>
              </article>
              {/*end item*/}
            </div>
          </div>
          {/*end of row*/}
        </div>
        {/*end of container*/}
      </Section>
      <Space />
      <Comments comments={data.comments} />
    </Layout>
  );
};

PagePoem.getInitialProps = async ({ query, req }) => {
  if (!req) {
    const data = await localforage.getItem<PoemPage>(`poem-${query.id}`);
    if (data) {
      return { data, pageID: query.id as string, availableOffline: true };
    }
  }
  const { data } = await instance.get<PoemPage>(`poem/${query.id}`);
  return { data, pageID: query.id as string, availableOffline: false };
};

export default PagePoem;
