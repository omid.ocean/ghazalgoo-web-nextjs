export interface Paginated<T> {
  nodes: T[];
  totalCount: number;
  pageInfo: PageInfo;
}

export interface PoetMainList {
  id: number;
  slogan: string;
  name: string;
  profile?: File;
}

export interface PageInfo {
  startCursor: number;
  endCursor: number;
  hasNextPage: boolean;
  hasPreviousPage: boolean;
}

export interface File {
  id: string;
  location: string;
  fileName: string;
  description: string;
  mimeType: string;
  size: number;
  creationDate: Date;
}

export interface PoetPage {
  id: number;
  slogan: string;
  name: string;
  biography: string;
  birthDate: Date;
  deathDate: Date;
  profile?: File;
  books: Book[];
}

export interface Book {
  id: number;
  slogan: string;
  name: string;
  description: string;
  creationDate: Date;
  cover?: File;
}

export interface BookPage {
  id: number;
  slogan: string;
  name: string;
  description?: string;
  creationDate: Date;
  cover?: File;
  poet: SubPoet;
  chapters: SubChapter[];
}

export interface SubPoet {
  id: number;
  slogan: string;
  name: string;
  biography: string;
  birthDate?: Date;
  deathDate?: Date;
  profile?: File;
}

export interface SubChapter {
  id: number;
  slogan: string;
  name: string;
  description?: string;
  creationDate: Date;
  poems: SubPoem[];
  poet: SubPoet;
}

export interface SubPoem {
  id: number;
  slogan: string;
  name: string;
  description?: any;
  creationDate: Date;
  cover?: any;
}

export interface SubVerse {
  id: number;
  order: number;
  isFirst: boolean;
  content: string;
  translation?: any;
}

export interface PoemPage {
  id: number;
  slogan: string;
  name: string;
  description?: string;
  creationDate: Date;
  cover?: File;
  chapter: SubChapter;
  verses: SubVerse[];
  tags: SubTag[];
  comments: SubComment[];
  likes: SubLike[];
}

export interface SubTag {
  id: number;
  content: string;
}

export interface SubComment {
  id: number;
  createdBy: any;
  creationDate: Date;
  comment: string;
  likes: SubLike[];
}

export interface SubLike {
  id: number;
  createdBy: any;

  creationDate: Date;
  action: any;
}
