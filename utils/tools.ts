import { SubVerse } from './types';

export const verseProcessor = (list: SubVerse[]) => {
  const matrix: [SubVerse, SubVerse?][] = [];
  const ordered = list.sort((a, b) =>
    a.order > b.order ? 1 : b.order > a.order ? -1 : 0
  );
  for (let i = 0; i < ordered.length; i = i + 2) {
    const item = ordered[i];
    const nextItem = ordered[i + 1];

    if (item.isFirst && !nextItem.isFirst) {
      matrix.push([item, nextItem]);
    } else if (item.isFirst && nextItem.isFirst) {
      matrix.push([item]);
      matrix.push([nextItem]);
    } else {
      matrix.push([item]);
      matrix.push([nextItem]);
    }
  }
  return matrix;
};
