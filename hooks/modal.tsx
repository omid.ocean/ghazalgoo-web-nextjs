import React, { useState } from 'react';

type Dispatch = {
  setOpenModal: React.Dispatch<
    React.SetStateAction<"createAcc" | "login" | undefined>
  >;
};
type State = { openModal: "createAcc" | "login" | undefined };
type ModalProviderProps = { children: React.ReactNode };
const ModalStateContext = React.createContext<State | undefined>(undefined);
const ModalDispatchContext = React.createContext<Dispatch | undefined>(
  undefined
);

const ModalProvider = ({ children }: ModalProviderProps) => {
  const [openModal, setOpenModal] = useState<"createAcc" | "login" | undefined>(
    undefined
  );

  return (
    <ModalStateContext.Provider value={{ openModal }}>
      <ModalDispatchContext.Provider value={{ setOpenModal }}>
        {children}
      </ModalDispatchContext.Provider>
    </ModalStateContext.Provider>
  );
};

const useModalState = () => {
  const context = React.useContext(ModalStateContext);
  if (context === undefined) {
    throw new Error("useSettingsState must be used within a ModalProvider");
  }
  return context;
};

const useModalDispatch = () => {
  const context = React.useContext(ModalDispatchContext);
  if (context === undefined) {
    throw new Error("useSettingsDispatch must be used within a ModalProvider");
  }
  return context;
};

export { ModalProvider, useModalState, useModalDispatch };
