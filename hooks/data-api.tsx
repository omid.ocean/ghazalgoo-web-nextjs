import { Dispatch, SetStateAction, useEffect, useReducer, useState } from 'react';

import { instance } from '../utils/axios';

type StateType<T> = {
  isLoading: boolean;
  isError: boolean;
  data: T | undefined;
};

type Action = {
  type: "FETCH_INIT" | "FETCH_SUCCESS" | "FETCH_FAILURE";
  payload?: any;
};

const dataFetchReducer = <T extends {}>(
  state: StateType<T>,
  action: Action
) => {
  switch (action.type) {
    case "FETCH_INIT":
      return {
        ...state,
        isLoading: true,
        isError: false
      };
    case "FETCH_SUCCESS":
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.payload
      };
    case "FETCH_FAILURE":
      return {
        ...state,
        isLoading: false,
        isError: true
      };
    default:
      throw new Error();
  }
};

const useDataApi = <T, U extends {}>(
  url: string,
  initialData?: T,
  params?: U
): [
  T | undefined,
  boolean,
  boolean,
  Dispatch<SetStateAction<U | undefined>>
] => {
  const [{ data, isLoading, isError }, dispatch] = useReducer<
    (state: StateType<T>, action: Action) => StateType<T>
  >(dataFetchReducer, {
    isLoading: false,
    isError: false,
    data: initialData
  });

  const [parameters, setParameters] = useState(params);

  useEffect(() => {
    let didCancel = false;
    const fetchData = async () => {
      dispatch({ type: "FETCH_INIT" });
      try {
        const result = await instance.get(url, { params: parameters });
        if (!didCancel) {
          dispatch({ type: "FETCH_SUCCESS", payload: result.data });
        }
      } catch (error) {
        if (!didCancel) {
          dispatch({ type: "FETCH_FAILURE" });
        }
      }
    };
    fetchData();
    return () => {
      didCancel = true;
    };
  }, [parameters]);

  return [data, isLoading, isError, setParameters];
};

export default useDataApi;
