import React from 'react';
import createPersistedState from 'use-persisted-state';

const languagePersistedState = createPersistedState("language");
const darkModePersistedState = createPersistedState("darkMode");

type Dispatch = {
  setLanguage: React.Dispatch<React.SetStateAction<string>>;
  setDarkMode: React.Dispatch<React.SetStateAction<boolean>>;
};
type State = { language: string; darkMode: boolean };
type SettingsProviderProps = { children: React.ReactNode };
const SettingsStateContext = React.createContext<State | undefined>(undefined);
const SettingsDispatchContext = React.createContext<Dispatch | undefined>(
  undefined
);

const SettingsProvider = ({ children }: SettingsProviderProps) => {
  const [language, setLanguage] = languagePersistedState("fa");
  const [darkMode, setDarkMode] = darkModePersistedState(false);

  return (
    <SettingsStateContext.Provider value={{ language, darkMode }}>
      <SettingsDispatchContext.Provider value={{ setLanguage, setDarkMode }}>
        {children}
      </SettingsDispatchContext.Provider>
    </SettingsStateContext.Provider>
  );
};

const useSettingsState = () => {
  const context = React.useContext(SettingsStateContext);
  if (context === undefined) {
    throw new Error("useSettingsState must be used within a SettingsProvider");
  }
  return context;
};

const useSettingsDispatch = () => {
  const context = React.useContext(SettingsDispatchContext);
  if (context === undefined) {
    throw new Error(
      "useSettingsDispatch must be used within a SettingsProvider"
    );
  }
  return context;
};

export { SettingsProvider, useSettingsState, useSettingsDispatch };
