import React from 'react';

import Feature from '@Components/common/feature/feature-small-8';
import Pagination from '@Components/common/pagination-alt/index';
import Section from '@Components/common/section';
import Spinner from '@Components/common/spinner';
import useDataApi from '@Hooks/data-api';
import { Paginated, PoetMainList } from '@Utils/types';

interface IArtistsList {}

const ArtistsList: React.FunctionComponent<IArtistsList> = ({}) => {
  const [data, isLoading, _isError, setParameters] = useDataApi<
    Paginated<PoetMainList>,
    { start?: number; end?: number; limit: number }
  >("poet", undefined, { limit: 12 });

  return (
    <Section>
      <div className="container" style={{ zIndex: 1, minHeight: "300px" }}>
        <h2>شعرا</h2>
        <p className="lead">درباره شعرا</p>
        <div className="row" style={{ position: "relative" }}>
          <Spinner active={isLoading} />

          {data &&
            data.nodes.map((val: PoetMainList) => {
              return (
                <Feature
                  key={val.slogan}
                  link={`/poet/[id]`}
                  as={`/poet/${val.slogan}`}
                  title={val.name}
                  image={
                    val.profile
                      ? val.profile.location
                      : "/static/images/artist-placeholder.jpg"
                  }
                />
              );
            })}
          {_isError}
        </div>
        <Pagination
          nextActive={data?.pageInfo.hasNextPage}
          prevActive={data?.pageInfo.hasPreviousPage}
          onNextClick={() =>
            setParameters({
              limit: 12,
              start: data?.pageInfo.endCursor,
              end: undefined
            })
          }
          onPrevClick={() =>
            setParameters({
              limit: 12,
              end: data?.pageInfo.startCursor,
              start: undefined
            })
          }
        />
      </div>
    </Section>
  );
};

export default ArtistsList;
