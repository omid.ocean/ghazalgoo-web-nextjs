import className from 'classnames';
import { useSettingsState } from 'hooks/settings';
import React from 'react';

import ActiveLink from './common/active-link';

interface IFooter {}

const Footer: React.FunctionComponent<IFooter> = () => {
  const { darkMode } = useSettingsState();

  return (
    <footer
      className={className("text-center space--sm footer-5", {
        "bg--dark": darkMode,
        "bg--secondary": !darkMode
      })}
    >
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="heading-block">
              <ul className="list-inline list--hover">
                <li>
                  <ActiveLink href="/">
                    <a>
                      <span>خانه</span>
                    </a>
                  </ActiveLink>
                </li>
                <li>
                  <ActiveLink href="/about">
                    <a>
                      <span>درباره پروژه</span>
                    </a>
                  </ActiveLink>
                </li>
                <li>
                  <ActiveLink href="/developers">
                    <a>
                      <span>توسعه دهندگان</span>
                    </a>
                  </ActiveLink>
                </li>
                <li>
                  <ActiveLink href="/donate">
                    <a>
                      <span>پشتیبانی از پروژه</span>
                    </a>
                  </ActiveLink>
                </li>
                <li>
                  <ActiveLink href="/contact">
                    <a>
                      <span>تماس با ما</span>
                    </a>
                  </ActiveLink>
                </li>
              </ul>
            </div>
            <div>
              <ul className="social-list list-inline list--hover">
                <li>
                  <a href="#">
                    <i className="icon-Linkedin icon icon--xs" />
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i className="icon-Instagram icon icon--xs" />
                  </a>
                </li>
              </ul>
            </div>
            <div>
              <span className="type--fine-print">
                توسعه و طراحی توسط امید آسترکی
              </span>
            </div>
            <div dir="ltr">
              <span className="type--fine-print">
                © <span className="update-year">2019</span> Ghazalgoo.ir.
              </span>
              <a className="type--fine-print" href="#">
                terms of use
              </a>
              <a className="type--fine-print" href="#">
                legal
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
