import styled from 'styled-components';

const LinkLookAlike = styled.span`
  cursor: pointer;
  color: #4a90e2;
  text-decoration: underline;
`;

export default LinkLookAlike;
