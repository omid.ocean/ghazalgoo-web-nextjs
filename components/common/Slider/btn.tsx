import React from 'react';

interface ISliderBtn {
  type: "next" | "previous";
  className?: string;
  style?: React.CSSProperties;
  onClick?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const SliderBtn: React.FunctionComponent<ISliderBtn> = ({
  type,
  style,
  onClick
}) => {
  return (
    <button
      className={`flickity-prev-next-button ${type}`}
      type="button"
      style={style}
      onClick={onClick}
    >
      <svg viewBox="0 0 100 100">
        <path
          d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z"
          className="arrow"
          transform={type === "next" ? "translate(100, 100) rotate(180)" : ""}
        />
      </svg>
    </button>
  );
};

export default SliderBtn;
