import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import { useSettingsState } from 'hooks/settings';
import React from 'react';
import SlickSlider from 'react-slick';

import Section from '@Components/common/section';
import SliderBtn from '@Components/common/Slider/btn';

interface ISlider {
  title: string;
  lead?: string;
  bg: "white" | "secondary" | "primary";
}

const settings = {
  slidesToShow: 4,
  slidesToScroll: 4,
  rows: 2,
  nextArrow: <SliderBtn type="next" />,
  prevArrow: <SliderBtn type="previous" />
};

const Slider: React.FunctionComponent<ISlider> = ({
  children,
  title,
  lead,
  bg
}) => {
  const { language } = useSettingsState();
  return (
    <Section bg={bg} className="text-center">
      <div className="container">
        <div className="row text-block">
          <div className="col-md-10 col-lg-8">
            <h3>{title}</h3>
            {lead && <p className="lead">{lead}</p>}
          </div>
        </div>
        <div className="slider slider--inline-arrows slider--arrows-hover">
          <SlickSlider {...settings} rtl={language === "fa"}>
            {children}
          </SlickSlider>
        </div>
      </div>
    </Section>
  );
};

export default Slider;
