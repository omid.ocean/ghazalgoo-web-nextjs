import styled from 'styled-components';

export const ModalCloseBtn = styled.div`
  cursor: pointer;
  position: absolute;
  opacity: 0.5;
  transition: 0.1s linear;
  -webkit-transition: 0.1s linear;
  -moz-transition: 0.1s linear;
  top: 1em;
  left: 1em;
  z-index: 99;
  &:before {
    content: "\\00D7";
    font-size: 1.5em;
  }
  &:hover {
    opacity: 1;
  }
`;
