import React from 'react';

import { useModalDispatch, useModalState } from 'hooks/modal';

import CreateAccountModal from './create-account';
import LoginModal from './login';

const ModalContainer: React.FunctionComponent = ({}) => {
  const { openModal } = useModalState();

  const { setOpenModal } = useModalDispatch();

  const close = () => {
    setOpenModal(undefined);
  };
  return (
    <div className="modal-content">
      <CreateAccountModal
        active={openModal === "createAcc"}
        background="/static/images/mainBG.jpg"
        close={close}
      />
      <LoginModal active={openModal === "login"} close={close} />
    </div>
  );
};

export default ModalContainer;
