import classNames from 'classnames';
import { useModalDispatch } from 'hooks/modal';
import { useSettingsState } from 'hooks/settings';
import React from 'react';

import BGImageHolder from '../backGround-image-holder';
import LinkLookAlike from '../link';
import { ModalCloseBtn } from './styled';

interface CreateAccountModalProps {
  readonly active: boolean;
  readonly background: string;
  readonly close: () => void;
}

const CreateAccountModal: React.FunctionComponent<CreateAccountModalProps> = ({
  active,
  background,
  close
}) => {
  const { darkMode } = useSettingsState();
  const { setOpenModal } = useModalDispatch();

  const openLogin = () => {
    setOpenModal("login");
  };
  return (
    <div
      className={classNames("modal-container", {
        "modal-active": active
      })}
      data-modal-index={2}
    >
      <div className="modal-content">
        <section className="imageblock feature-large bg--white border--round ">
          <div className="imageblock__content col-lg-5 col-md-3 pos-right">
            <BGImageHolder
              background={background}
              fullWidth={true}
              alt="sign up"
            />
          </div>
          <div
            className={classNames("container", {
              "bg--dark": darkMode
            })}
          >
            <div className="row justify-content-end">
              <div className="col-lg-6 col-md-7">
                <div className="row">
                  <div className="col-md-11 col-lg-10">
                    <h1>ثبت نام کاربر جدید</h1>
                    <p className="lead">برای عضویت فرم زیر را تکمیل کنید</p>
                    <hr className="short" />
                    <form>
                      <div className="row">
                        <div className="col-12">
                          <input
                            type="email"
                            name="email"
                            placeholder="آدرس ایمیل"
                          />
                        </div>
                        <div className="col-12">
                          <input
                            type="password"
                            name="Password"
                            placeholder="پسورد"
                          />
                        </div>
                        <div className="col-12">
                          <button type="submit" className="btn btn--primary">
                            ایجاد اکانت
                          </button>
                        </div>
                        <div className="col-12">
                          <span className="type--fine-print">
                            با ثبت نام شما با <a href="#"> قوانین </a> موافقت
                            میکنید.
                          </span>
                          <span className="type--fine-print block">
                            قبلا ثبت نام کرده اید؟{" "}
                            <LinkLookAlike onClick={() => openLogin()}>
                              ورود
                            </LinkLookAlike>
                          </span>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <ModalCloseBtn onClick={() => close()} />
      </div>
    </div>
  );
};

export default CreateAccountModal;
