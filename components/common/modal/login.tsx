import classNames from 'classnames';
import { useSettingsState } from 'hooks/settings';
import React from 'react';

import { useModalDispatch } from '@Hooks/modal';

import LinkLookAlike from '../link';
import { ModalCloseBtn } from './styled';

interface LoginModalProps {
  readonly active: boolean;
  readonly close: () => void;
}

const LoginModal: React.FunctionComponent<LoginModalProps> = ({
  active,
  close
}) => {
  const { darkMode } = useSettingsState();
  const { setOpenModal } = useModalDispatch();

  const openCreateAcc = () => {
    setOpenModal("createAcc");
  };

  return (
    <div
      className={classNames("modal-container", {
        "modal-active": active
      })}
      data-modal-index={2}
    >
      <div className="modal-content section-modal">
        <section className="unpad ">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-6 col-12">
                <div
                  className={classNames("feature feature-1", {
                    "bg--dark": darkMode
                  })}
                >
                  <div className="feature__body boxed boxed--lg boxed--border text-center">
                    <ModalCloseBtn onClick={() => close()} />
                    <div className="text-block">
                      <h3>وارد اکانت خود شوید</h3>
                      <p>لطفا وارد اکانت خود شوید</p>
                    </div>
                    <form>
                      <div className="row">
                        <div className="col-md-12">
                          <input type="text" placeholder="نام کاربری" />
                        </div>
                        <div className="col-md-12">
                          <input type="password" placeholder="کلمه عبور" />
                        </div>
                        <div className="col-md-12">
                          <button className="btn btn--primary" type="submit">
                            ورود
                          </button>
                        </div>
                      </div>
                    </form>
                    <span className="type--fine-print block">
                      هنوز اکانت ندارید؟{" "}
                      <LinkLookAlike onClick={() => openCreateAcc()}>
                        ایجاد اکانت
                      </LinkLookAlike>
                    </span>
                    <span className="type--fine-print block">
                      کلمه عبور خود را فراموش کرده اید؟{" "}
                      <LinkLookAlike onClick={() => openCreateAcc()}>
                        بازیابی کلمه عبور
                      </LinkLookAlike>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

export default LoginModal;
