import cn from 'classnames';
import { useSettingsState } from 'hooks/settings';
import React from 'react';
import styled from 'styled-components';
import { ICommonComponent } from 'utils/common';

interface ISection extends ICommonComponent {
  readonly className?: string;
  readonly colors?: [string, string, string, string];
}

interface IStyledSection {
  readonly isGradient: boolean;
  readonly colors: [string, string, string, string];
}

const StyledSection = styled.section<IStyledSection>`
  ${props => {
    if (props.isGradient) {
      return `
  background: linear-gradient(-45deg, ${props.colors[0]}, ${props.colors[1]}, ${props.colors[2]}, ${props.colors[3]});
  background-size: 400% 400%;
  -webkit-animation: Gradient 15s ease infinite;
  -moz-animation: Gradient 15s ease infinite;
  animation: Gradient 15s ease infinite;
`;
    } else {
      return ``;
    }
  }}
`;

const Section: React.FunctionComponent<ISection> = ({
  className,
  bg,
  children,
  colors = ["#e77552", "#e73c7e", "#23a6d5", "#23d5ab"],
  ...props
}) => {
  const { darkMode } = useSettingsState();

  return (
    <StyledSection
      className={cn(className, {
        "bg--dark": darkMode || bg === "animated",
        "bg--primary": !darkMode && bg === "primary",
        "bg--secondary": !darkMode && bg === "secondary"
      })}
      isGradient={bg === "animated"}
      colors={colors}
      {...props}
    >
      {children}
    </StyledSection>
  );
};

export default Section;
