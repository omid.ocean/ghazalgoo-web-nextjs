import React from 'react';

import Section from '@Components/common/section';

interface ISpace {}

const Space: React.FunctionComponent<ISpace> = ({}) => {
  return (
    <Section className="space--sm">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <hr />
          </div>
        </div>
      </div>
    </Section>
  );
};

export default Space;
