import React from 'react';

import Section from '@Components/common/section';

import { SubComment } from '../../../utils/types';
import Comment from './comment';
import NewComment from './new-comment';

interface IComments {
  comments: SubComment[];
}

const Comments: React.FunctionComponent<IComments> = ({ comments }) => {
  return (
    <Section bg="white">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-10 col-lg-8">
            <div className="comments">
              <h3>{comments.length} نظر ثبت شده</h3>
              <ul className="comments__list">
                {comments.map(val => (
                  <li>
                    <Comment comment={val} />
                  </li>
                ))}
              </ul>
            </div>
            {/*end comments*/}
            <NewComment />
          </div>
        </div>
      </div>
    </Section>
  );
};

export default Comments;
