import React from 'react';

import { SubComment } from '../../../utils/types';

interface IComment {
  comment: SubComment;
}

const Comment: React.FunctionComponent<IComment> = ({ comment }) => {
  return (
    <div className="comment">
      <div className="comment__avatar">
        <img alt="Image" src={comment.createdBy} />
      </div>
      <div className="comment__body">
        <h5 className="type--fine-print">{comment.createdBy}</h5>
        <div className="comment__meta">
          <span>{comment.creationDate}</span>
          <a href="#">پاسخ</a>
        </div>
        <p>{comment.comment}</p>
      </div>
    </div>
  );
};

export default Comment;
