import React from 'react';

interface INewComment {}

const NewComment: React.FunctionComponent<INewComment> = ({}) => {
  return (
    <div className="comments-form">
      <h4>نظر بدهید!</h4>
      <form className="row">
        <div className="col-md-6">
          <label>نام شما:</label>
          <input
            type="text"
            name="Name"
            placeholder="نام خود را اینجا وارد نمایید"
          />
        </div>
        <div className="col-md-6">
          <label>آدرس ایمیل:</label>
          <input type="email" name="email" placeholder="آدر ایمیل" />
        </div>
        <div className="col-md-12">
          <label>نظر:</label>
          <textarea
            rows={4}
            name="Message"
            placeholder="پیام"
            defaultValue={""}
          />
        </div>
        <div className="col-md-3">
          <button className="btn btn--primary" type="submit">
            ثبت نظر
          </button>
        </div>
      </form>
    </div>
  );
};

export default NewComment;
