import React from 'react';

export interface ITab {
  title?: string;
  icon?: string;
  children: JSX.Element;
}

const Tab: React.FunctionComponent<ITab> = ({}) => {
  return <></>;
};

export default Tab;
