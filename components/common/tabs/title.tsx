import className from 'classnames';
import React from 'react';

interface ITitle {
  icon?: string;
  title?: string;
  isActive: boolean;
  onClick: () => void;
}

const Title: React.FunctionComponent<ITitle> = ({
  icon,
  title,
  isActive,
  onClick
}) => {
  return (
    <li
      className={className({
        active: isActive
      })}
      onClick={() => onClick()}
    >
      <div className="tab__title">
        {icon && <i className="icon icon--sm block icon-Target-Market" />}
        {title && <span className="h5">{title}</span>}
      </div>
    </li>
  );
};

export default Title;
