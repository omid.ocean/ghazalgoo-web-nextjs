import className from 'classnames';
import React, { useState } from 'react';
import { ICommonComponent } from 'utils/common';

import Section from '../section';
import Content from './content';
import { ITab } from './tab';
import Title from './title';

interface ITabs extends ICommonComponent {
  children: React.ReactElement<ITab>[];
  type: "vertical" | "horizontal";
}

const Tabs: React.FunctionComponent<ITabs> = ({ children, bg, type }) => {
  const [activeTab, setActiveTab] = useState(0);

  const onClick = (index: number) => setActiveTab(index);
  return (
    <Section
      bg={bg}
      className={className({
        "text-center": type === "horizontal"
      })}
    >
      <div className="container">
        <div className="row justify-content-center">
          <div
            className={className({
              "col-md-8": type === "horizontal",
              "col-md-10": type === "vertical"
            })}
          >
            <div
              className={className("tabs-container", {
                "tabs--vertical": type === "vertical"
              })}
            >
              <ul className="tabs">
                {children.map((val, index) => (
                  <Title
                    icon={val.props.title}
                    title={val.props.title}
                    isActive={activeTab === index}
                    key={index}
                    onClick={onClick.bind(undefined, index)}
                  />
                ))}
              </ul>
              <ul className="tabs-content">
                {children.map((val, index) => (
                  <Content
                    content={val.props.children}
                    isActive={activeTab === index}
                    key={index}
                  />
                ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </Section>
  );
};

export default Tabs;
