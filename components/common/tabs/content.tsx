import className from 'classnames';
import React from 'react';

interface IContent {
  content: JSX.Element;
  isActive: boolean;
}

const Content: React.FunctionComponent<IContent> = ({ content, isActive }) => {
  return (
    <li
      className={className({
        active: isActive
      })}
    >
      <div className="tab__content">{content}</div>
    </li>
  );
};

export default Content;
