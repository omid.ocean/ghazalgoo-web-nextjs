import Link from 'next/link';
import React from 'react';

import { IPage } from './page';

interface IPagination {
  children: React.ReactElement<IPage>[];
  prevURL: string;
  nextURL: string;
}

const Pagination: React.FunctionComponent<IPagination> = ({
  children,
  prevURL,
  nextURL
}) => {
  return (
    <div className="pagination" dir="ltr">
      <Link href={prevURL}>
        <a className="pagination__prev" title="Previous Page">
          «
        </a>
      </Link>
      <ol>{children}</ol>
      <Link href={nextURL}>
        <a className="pagination__next" title="Next Page">
          »
        </a>
      </Link>
    </div>
  );
};

export default Pagination;
