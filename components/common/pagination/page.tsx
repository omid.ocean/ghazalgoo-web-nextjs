import classNames from 'classnames';
import Link from 'next/link';
import React from 'react';

export interface IPage {
  pageNumber: string;
  url: string;
  isActive: boolean;
}

const Page: React.FunctionComponent<IPage> = ({
  pageNumber,
  url,
  isActive
}) => {
  return (
    <li className={classNames({ pagination__current: isActive })}>
      {isActive ? (
        <>{pageNumber}</>
      ) : (
        <Link href={url}>
          <a>{pageNumber}</a>
        </Link>
      )}
    </li>
  );
};

export default Page;
