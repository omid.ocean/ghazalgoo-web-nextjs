import React from 'react';
import { ICommonComponent } from 'utils/common';

import Section from '@Components/common/section';

interface IAvatarCover extends ICommonComponent {
  name: string;
  desc?: string;
  avatar?: string;
  colors?: [string, string, string, string];
}

const AvatarCover: React.FunctionComponent<IAvatarCover> = ({
  name,
  desc,
  avatar = "/static/images/artist-placeholder.jpg",
  bg = "animated",
  colors = ["#09203f", "#537895", "#29323c", "#29323c"]
}) => {
  return (
    <Section
      className="cover text-center"
      data-gradient-bg="#4876BD,#5448BD,#8F48BD,#BD48B1"
      bg={bg}
      colors={colors}
    >
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-12">
            <div className="avatar border--round">
              <img
                className="rounded-circle box-shadow-wide"
                alt={name}
                src={avatar}
              />
            </div>
          </div>

          <div className="col-md-8 col-lg-6 data">
            <h1>{name}</h1>
            {desc && <p className="lead">{desc}</p>}
          </div>
        </div>
      </div>
    </Section>
  );
};

export default AvatarCover;
