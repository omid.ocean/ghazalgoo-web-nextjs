import React from 'react';
import { ICommonComponent } from 'utils/common';

import Section from '@Components/common/section';

interface IImageCover extends ICommonComponent {
  title: string;
  lead?: string;
  button?: {
    title: string;
    url: string;
  };
  images: [string, string, string];
}

const ImageCover: React.FunctionComponent<IImageCover> = ({
  title,
  lead,
  button,
  images,
  bg = "secondary"
}) => {
  return (
    <Section className="cover text-center" bg={bg}>
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-8 col-lg-6">
            <h1>{title}</h1>
            {lead && <p className="lead">{lead}</p>}
            {button && (
              <a className="btn btn--primary type--uppercase" href={button.url}>
                <span className="btn__text">{button.title}</span>
              </a>
            )}
          </div>
          <div className="col-md-12">
            <div className="triptych border--round">
              <img alt="Image" src={images[0]} />
              <img alt="Image" src={images[1]} />
              <img alt="Image" src={images[2]} />
            </div>
          </div>
        </div>
      </div>
    </Section>
  );
};

export default ImageCover;
