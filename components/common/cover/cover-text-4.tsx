import Link from 'next/link';
import React from 'react';

import Section from '@Components/common/section';

interface ICover {
  coverImage: string;
  title: string;
  content: string;
  button?: { title: string; link: string };
  bg: "white" | "secondary" | "primary";
}

const Cover: React.FunctionComponent<ICover> = ({
  coverImage,
  title,
  content,
  button,
  bg
}) => {
  return (
    <Section className="imageblock switchable height-80" bg={bg}>
      <div className="imageblock__content col-lg-6 col-md-4 pos-right">
        <div
          className="background-image-holder"
          style={{ background: `url("${coverImage}")`, opacity: 1 }}
        ></div>
      </div>
      <div className="container pos-vertical-center">
        <div className="row">
          <div className="col-lg-5 col-md-7">
            <h1>{title}</h1>
            <p className="lead">{content}</p>
            {button && (
              <Link href={button.link}>
                <a className="btn btn--primary type--uppercase">
                  <span className="btn__text">{button.title}</span>
                </a>
              </Link>
            )}
          </div>
        </div>
      </div>
    </Section>
  );
};

export default Cover;
