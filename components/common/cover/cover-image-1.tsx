import React from 'react';

interface IImageCover {
  title: string;
  lead?: string;
  short?: string;
  backGround?: string;
}

const ImageCover: React.FunctionComponent<IImageCover> = ({
  title,
  lead,
  short,
  backGround = "/static/images/bookBG.jpg"
}) => {
  return (
    <section className="imagebg image--light cover cover-blocks bg--secondary ">
      <div
        className="background-image-holder hidden-xs"
        style={{ background: `url("${backGround}")`, opacity: 1 }}
      >
        <img alt="background" src={backGround} />
      </div>
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-lg-5 ">
            <div>
              <h1>{title}</h1>
              <p className="lead">{lead}</p>
              {short && (
                <>
                  <hr className="short" />
                  <p>{short}</p>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ImageCover;
