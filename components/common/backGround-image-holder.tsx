import styled from 'styled-components';

interface IBGImageHolder {
  readonly background?: string;
  readonly alt?: string;
  fullWidth?: boolean;
}

const StyledBGImageHolder = styled.div<{
  background?: string;
  fullWidth: boolean;
}>`
  background: ${props =>
    props.background ? `url(${props.background})` : "#252525"};
  opacity: ${props => (props.background ? 1 : 0)};
  position: absolute;
  height: 100%;
  top: 0;
  left: 0;
  background-size: cover !important;
  background-position: 50% 50% !important;
  z-index: 0;
  transition: opacity 0.3s linear;
  -webkit-transition: opacity 0.3s linear;
  ${props => (props.fullWidth ? "width: 100%;" : "")}
  img {
    display: none;
  }
  z-index: inherit !important;
`;

const BGImageHolder: React.FunctionComponent<IBGImageHolder> = ({
  background,
  alt,
  fullWidth = false,
  ...props
}) => {
  return (
    <StyledBGImageHolder
      background={background}
      fullWidth={fullWidth}
      {...props}
    >
      {background && <img alt={alt} src={background} />}
    </StyledBGImageHolder>
  );
};

export default BGImageHolder;
