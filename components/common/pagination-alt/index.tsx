import classnames from 'classnames';
import React from 'react';

import StyledPagination from './styled';

interface IPagination {
  onNextClick: () => void;
  onPrevClick: () => void;
  nextActive?: boolean;
  prevActive?: boolean;
}

const Pagination: React.FunctionComponent<IPagination> = ({
  onNextClick,
  onPrevClick,
  nextActive,
  prevActive
}) => {
  return (
    <StyledPagination>
      <ul>
        <li
          className={classnames({
            "is-active": prevActive
          })}
          onClick={() => {
            if (prevActive) {
              onPrevClick();
            }
          }}
        >
          قبلی
        </li>
        <li
          className={classnames({
            "is-active": nextActive
          })}
          onClick={() => {
            if (nextActive) {
              onNextClick();
            }
          }}
        >
          بعدی
        </li>
      </ul>
    </StyledPagination>
  );
};

export default Pagination;
