import styled from 'styled-components';

const StyledPagination = styled.div`
  padding: 30px 0;
  direction: ltr;
  text-align: center;
  cursor: default;
  ul {
    margin: 0;
    padding: 0;
    list-style-type: none;
  }

  li {
    display: inline-block;
    padding: 7px 12px;
    color: #222;
  }

  li {
    background-color: #2ecc71;
    margin: auto 5px;
    color: #fff;
    border: 3px solid #2ecc71;
    position: relative;
  }

  li:first-of-type:before {
    content: "";
    position: absolute;
    top: -3px;
    left: -25px;
    border-top: 22px solid transparent;
    border-bottom: 22px solid transparent;
    border-right: 22px solid #2ecc71;
  }

  li:last-of-type:after {
    content: "";
    position: absolute;
    top: -3px;
    right: -25px;
    border-top: 22px solid transparent;
    border-bottom: 22px solid transparent;
    border-left: 22px solid #2ecc71;
  }

  .is-active {
    font-weight: bold;
    cursor: pointer;
  }
`;

export default StyledPagination;
