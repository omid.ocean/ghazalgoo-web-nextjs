import React from 'react';

interface IIconButton {
  link?: string;
  title: string;
  iconClass: string;
  className?: string;
  onClick?: () => void;
}

const IconButton: React.FunctionComponent<IIconButton> = ({
  title,
  iconClass,
  className,
  onClick
}) => {
  return (
    <span
      className={`btn btn--primary btn--icon ${className}`}
      onClick={() => onClick && onClick()}
    >
      <span className="btn__text">
        <i className={iconClass} />
        {title}
      </span>
    </span>
  );
};

export default IconButton;
