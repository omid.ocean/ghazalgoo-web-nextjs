import React from 'react';

import Section from '../section';

interface IPageTitle {
  title: string;
  lead?: string;
  bg?: "white" | "secondary" | "primary";
}

const PageTitle: React.FunctionComponent<IPageTitle> = ({
  title,
  lead,
  bg
}) => {
  return (
    <Section className="text-center" bg={bg}>
      <div className="container">
        <div className="row">
          <div className="col-md-10 col-lg-8">
            <h1>{title}</h1>
            {lead && <p className="lead">{lead}</p>}
          </div>
        </div>
      </div>
    </Section>
  );
};

export default PageTitle;
