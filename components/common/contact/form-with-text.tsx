import React from 'react';

import Section from '@Components/common/section';

interface IContact {
  email?: string;
  phone?: string;
  desc: string;
  time: string;
  bg?: "white" | "secondary" | "primary";
}

const Contact: React.FunctionComponent<IContact> = ({
  email,
  phone,
  desc,
  time,
  bg
}) => {
  return (
    <Section className="switchable" bg={bg}>
      <div className="container">
        <div className="row justify-content-between">
          <div className="col-md-5">
            <p className="lead">
              {email && (
                <>
                  ایمیل: <a href={`mailto:${email}`}>{email}</a>
                </>
              )}
              {phone && (
                <>
                  <br /> تلفن: {phone}
                </>
              )}
            </p>
            <p className="lead"> {desc} </p>
            <p className="lead"> {time}</p>
          </div>
          <div className="col-md-6 col-12">
            <form className="form-email row mx-0">
              <div className="col-md-6 col-12">
                <label>نام شما:</label>{" "}
                <input type="text" name="name" className="validate-required" />
              </div>
              <div className="col-md-6 col-12">
                <label>آدرس ایمیل:</label>
                <input
                  type="email"
                  name="email"
                  className="validate-required"
                />
              </div>
              <div className="col-md-12 col-12">
                <label>پیام:</label>
                <textarea
                  rows={4}
                  name="Message"
                  className="validate-required"
                  defaultValue=""
                />
              </div>
              <div className="col-md-5 col-lg-4 col-6">
                <button
                  type="submit"
                  className="btn btn--primary type--uppercase"
                >
                  ارسال پیام
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Section>
  );
};

export default Contact;
