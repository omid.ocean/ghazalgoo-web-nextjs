import Link from 'next/link';
import React from 'react';

import Section from '@Components/common/section';

interface IRaw {
  title: string;
  lead: string;
  image: string;
  bg?: "white" | "secondary" | "primary";
  button?: {
    title: string;
    url: string;
  };
}

const FeatureLarge: React.FunctionComponent<IRaw> = ({
  title,
  lead,
  image,
  button,
  bg
}) => {
  return (
    <Section bg={bg} className="switchable feature-large">
      <div className="container">
        <div className="row justify-content-around">
          <div className="col-md-6">
            <img
              alt={title}
              className="border--round box-shadow-wide"
              src={image}
            />
          </div>
          <div className="col-md-6 col-lg-5">
            <div className="switchable__text">
              <h2>{title}</h2>
              <p className="lead">{lead}</p>
              {button && (
                <Link href={button.url}>
                  <a>{button.title}</a>
                </Link>
              )}
            </div>
          </div>
        </div>
      </div>
    </Section>
  );
};

export default FeatureLarge;
