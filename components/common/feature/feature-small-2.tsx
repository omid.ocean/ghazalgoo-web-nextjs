import Link from 'next/link';
import React from 'react';
import styled from 'styled-components';

interface IFeature {
  title?: string;
  icon?: string;
  number?: number;
  desc?: string;
  linked: {
    link: string;
    as: string;
  };
}

const Number = styled.p`
  font-size: 2em;
  float: right;
  width: 25%;
  color: #4a90e2 !important;
  margin: 0;
`;

const Feature: React.FunctionComponent<IFeature> = ({
  title,
  icon,
  number,
  desc,
  linked
}) => {
  return (
    <div className="col-md-4">
      <Link href={linked.link} as={linked.as}>
        <a>
          <div className="feature feature-2 boxed boxed--border">
            {icon && <i className={`icon ${icon}`} />}
            {number && <Number>{number} </Number>}
            <div className="feature__body">
              {title && <h5>{title}</h5>}
              {desc && <p>{desc}</p>}
            </div>
          </div>
        </a>
      </Link>
    </div>
  );
};

export default Feature;
