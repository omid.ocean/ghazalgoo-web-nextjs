import Link from 'next/link';
import React from 'react';

import BGImageHolder from '../backGround-image-holder';

interface IFeature {
  title: string;
  image: string;
  link: string;
  as?: string;
}

const Feature: React.FunctionComponent<IFeature> = ({
  title,
  image,
  link,
  as
}) => {
  return (
    <div className="col-6 col-sm-4 col-md-3 col-lg-2">
      <Link href={link} as={as}>
        <a className="block">
          <div
            className="feature feature-7 boxed text-center imagebg"
            data-overlay={3}
          >
            <BGImageHolder alt="artist" background={image} fullWidth={true} />
            <h4 className="pos-vertical-center">{title}</h4>
          </div>
        </a>
      </Link>
    </div>
  );
};

export default Feature;
