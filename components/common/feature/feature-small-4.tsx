import Link from 'next/link';
import React from 'react';

interface IFeature {
  title: string;
  icon?: string;
  desc?: string;
  button?: {
    title: string;
    link: string;
    as: string;
  };
}

const Feature: React.FunctionComponent<IFeature> = ({
  title,
  icon,
  desc,
  button
}) => {
  return (
    <div className="col-md-3">
      <div className="feature feature-4 boxed boxed--lg boxed--border">
        {icon && <i className={`icon ${icon}`} />}
        <h4 style={{ marginBottom: desc ? "0.342105em" : "2.342105em" }}>
          {title}
        </h4>

        {desc && (
          <>
            <hr /> <p>{desc}</p>
          </>
        )}
        {button && (
          <Link href={button.link} as={button.as}>
            <a className="btn btn--primary">
              <span className="btn__text">{button.title}</span>
            </a>
          </Link>
        )}
      </div>
    </div>
  );
};

export default Feature;
