import { WithRouterProps } from 'next/dist/client/with-router';
import Link from 'next/link';
import { withRouter } from 'next/router';
import { ReactElementLike } from 'prop-types';
import React, { Children } from 'react';

interface ActiveLinkProps extends WithRouterProps {
  href: string;
  children: ReactElementLike;
  activeClassName?: string;
  as?: string;
}

const ActiveLink: React.ComponentClass<
  Pick<ActiveLinkProps, "children" | "href" | "as" | "activeClassName">,
  any
> = withRouter(
  ({
    router,
    children,
    as,
    href,
    activeClassName = "link--active",
    ...rest
  }) => (
    <Link {...rest} href={href} as={as}>
      {React.cloneElement(Children.only(children), {
        className:
          router.asPath === href || router.asPath === as
            ? activeClassName
            : null
      })}
    </Link>
  )
);

export default ActiveLink;
