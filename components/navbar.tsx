import classNames from 'classnames';
import { useModalDispatch } from 'hooks/modal';
import { useSettingsDispatch, useSettingsState } from 'hooks/settings';
import Link from 'next/link';
import React, { useState } from 'react';

import ActiveLink from './common/active-link';

interface INavBar {}

const NavBar: React.FunctionComponent<INavBar> = () => {
  const { darkMode } = useSettingsState();
  const { setDarkMode } = useSettingsDispatch();

  const { setOpenModal } = useModalDispatch();

  const [mobileVisible, setMobileVisible] = useState(false);

  const openLoginModal = () => {
    setOpenModal("login");
  };

  const changeVisibility = () => {
    setMobileVisible(!mobileVisible);
  };
  return (
    <div className="nav-container">
      <div>
        <div
          className={classNames("bar bar--sm visible-xs", {
            "bg--dark": darkMode
          })}
        >
          <div className="container">
            <div className="row">
              <div className="col-3 col-md-2">
                <Link href="/">
                  <a>
                    <img
                      className="logo logo-dark"
                      alt="logo"
                      src="/static/images/logo-2-dark.png"
                    />
                    <img
                      className="logo logo-light"
                      alt="logo"
                      src="/static/images/logo-2-light.png"
                    />
                  </a>
                </Link>
              </div>
              <div className="col-9 col-md-10 text-right">
                <i
                  onClick={changeVisibility}
                  className="icon icon--sm stack-interface stack-menu"
                />
              </div>
            </div>
          </div>
        </div>
        <nav
          id="menu2"
          className={classNames("bar bar-2", {
            "bg--dark": darkMode,
            "hidden-xs": !mobileVisible
          })}
        >
          <div className="container">
            <div className="row">
              <div
                className={classNames(
                  "col-lg-2 text-center text-left-sm order-lg-2 hidden-xs"
                )}
              >
                <div className="bar__module">
                  <Link href="/">
                    <a>
                      <img
                        className="logo logo-dark"
                        alt="logo"
                        src="/static/images/logo-2-dark.png"
                      />
                      <img
                        className="logo logo-light"
                        alt="logo"
                        src="/static/images/logo-2-light.png"
                      />
                    </a>
                  </Link>
                </div>
              </div>
              <div className="col-lg-5 order-lg-1">
                <div className="bar__module">
                  <ul className="menu-horizontal text-left">
                    <li>
                      <ActiveLink href="/">
                        <a>خانه</a>
                      </ActiveLink>
                    </li>
                    <li>
                      <ActiveLink href="/about">
                        <a>درباره پروژه</a>
                      </ActiveLink>
                    </li>
                    <li>
                      <ActiveLink href="/donate">
                        <a>پشتیبانی از پروژه</a>
                      </ActiveLink>
                    </li>
                    <li>
                      <ActiveLink href="/contact">
                        <a href="#">تماس با ما</a>
                      </ActiveLink>
                    </li>
                    <li>
                      <a
                        onClick={() => {
                          setDarkMode(!darkMode);
                        }}
                      >
                        حالت تاریک
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-5 text-right-xs text-right text-right-sm order-lg-3 bar__module">
                <div className="bar__module">
                  <a
                    className="btn btn--sm type--uppercase"
                    onClick={() => openLoginModal()}
                  >
                    <span className="btn__text">ورود</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </div>
  );
};

export default NavBar;
