// import '../tools/loader';
// import '../tools/scripts';

// import '../tools/smooth-scroll';
import FontFaceObserver from 'fontfaceobserver';
import { useSettingsState } from 'hooks/settings';
import React from 'react';

interface ITheme {}

const Theme: React.FunctionComponent<ITheme> = ({}) => {
  const { language } = useSettingsState();
  if (language === "fa" && typeof window !== "undefined") {
    document.body.setAttribute("dir", "rtl");

    new FontFaceObserver("iranyekan").load().then(() => {
      document.body.classList.add("font-loaded");
    });
  }

  return <></>;
};

export default Theme;
