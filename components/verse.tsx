import classnames from 'classnames';
import React from 'react';

import { SubVerse } from '@Utils/types';

import { verseProcessor } from '../utils/tools';

interface IVerse {
  verses: SubVerse[];
}

const Verses: React.FunctionComponent<IVerse> = ({ verses }) => {
  const processed = verseProcessor(verses);
  return (
    <>
      {processed.map((val, index) => {
        return (
          <div className="row" key={index}>
            <div
              className={classnames("col", {
                "col-md-6 text-right": !!val[1],
                "text-center": !val[1]
              })}
            >
              {val[0]?.content}
            </div>
            {val[1] && (
              <div className="col-md-6 text-left">{val[1]?.content}</div>
            )}
          </div>
        );
      })}
    </>
  );
};

export default Verses;
