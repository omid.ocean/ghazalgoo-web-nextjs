import React from 'react';

interface IChapterItem {
  image: string;
  title: string;
  subTitle: string;
}

const ChapterItem: React.FunctionComponent<IChapterItem> = ({
  image,
  title,
  subTitle
}) => {
  return (
    <div className=" masonry__item col-lg-3 col-md-6">
      <div className="project-thumb hover-element hover--active border--round">
        <a href="#">
          <div className="hover-element__initial">
            <div
              className="background-image-holder"
              style={{ background: `url("${image}")`, opacity: 1 }}
            >
              <img alt={title} src={image} />
            </div>
          </div>
          <div className="hover-element__reveal" data-scrim-top="5">
            <div className="project-thumb__title">
              <h4>{title}</h4>
              <span>{subTitle}</span>
            </div>
          </div>
        </a>
      </div>
    </div>
  );
};

export default ChapterItem;
