import Link from 'next/link';
import React from 'react';

export interface IArtist {
  name: string;
  desc: string;
  avatar: string;
  link: string;
}

const Artist: React.FunctionComponent<IArtist> = ({
  name,
  desc,
  avatar,
  link,
  ...props
}) => {
  return (
    <div className="feature feature-8 " {...props}>
      <Link href={link}>
        <a>
          <img
            className="rounded-circle mx-auto d-block"
            alt={name}
            src={avatar}
          />
          <h5>{name}</h5> <span>{desc}</span>
        </a>
      </Link>
    </div>
  );
};

export default Artist;
