import '@Styles/bootstrap.css';
import '@Styles/iconsmind.css';
import '@Styles/persian-font.css';
import '@Styles/stack-interface.css';
import '@Styles/theme-rtl.css';
import '@Styles/theme.css';
import '@Styles/custom.css';

import Head from 'next/head';
import React from 'react';

interface LayoutProps {
  title: string;
}

const Layout: React.FunctionComponent<LayoutProps> = ({ children, title }) => {
  return (
    <>
      <Head>
        <title>{title}</title>
      </Head>
      <div className="main-container">{children}</div>
    </>
  );
};

export default Layout;
