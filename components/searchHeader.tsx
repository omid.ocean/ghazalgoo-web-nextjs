import { useSettingsState } from 'hooks/settings';
import React, { useState } from 'react';

import BGImageHolder from './common/backGround-image-holder';

interface IHeader {
  title: string;
  subTitle: string;
  onSubmit: (input: string) => void;
  onChange?: (input: string) => void;
}

const HomeHeader: React.FunctionComponent<IHeader> = ({
  title,
  subTitle,
  onSubmit
}) => {
  const [input, setInput] = useState("");
  const { darkMode } = useSettingsState();

  const onChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInput(event.target.value);
  };

  const submitHandler = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    onSubmit(input);
  };

  return (
    <section className="cover imagebg height-70 text-center" data-overlay={4}>
      <BGImageHolder
        alt="background"
        background="/static/images/mainBG.jpg"
        fullWidth={true}
      />
      <div className="container pos-vertical-center">
        <div className="row">
          <div className="col-md-10 col-lg-8">
            <h1>{title}</h1>
            <p className="lead">{subTitle}</p>
            <div
              className={`boxed boxed--lg bg--${
                darkMode ? "dark" : "white"
              } text-left`}
            >
              <form
                className="form--horizontal row m-0"
                onSubmit={submitHandler}
              >
                <div className="col-md-8">
                  <input
                    type="text"
                    name="search"
                    placeholder="کلمات کلیدی خود را وارد نمایید"
                    value={input}
                    onChange={onChangeHandler}
                  />
                </div>
                <div className="col-md-4">
                  <button
                    type="submit"
                    className="btn btn--primary type--uppercase"
                  >
                    بیاب
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default HomeHeader;
