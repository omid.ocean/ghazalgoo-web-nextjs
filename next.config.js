const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

const withPlugins = require("next-compose-plugins");
const withCSS = require("@zeit/next-css");
const withOffline = require("next-offline");

module.exports = withOffline(
  withPlugins(
    [
      [
        withCSS,
        {
          cssLoaderOptions: {
            url: false
          }
        }
      ]
    ],
    {
      webpack: (config, _options) => {
        if (config.resolve.plugins) {
          config.resolve.plugins.push(new TsconfigPathsPlugin());
        } else {
          config.resolve.plugins = [new TsconfigPathsPlugin()];
        }

        return config;
      },
      target: "serverless"
    }
  )
);
